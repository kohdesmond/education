//https://stackoverflow.com/questions/22997516/to-find-all-emails-on-a-page
function checkEmail(notification) 
{
 var emailFormat = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi

    string_context = notification.toString(); //Return String object
    array_mails = string_context.match(emailFormat);
    console.log('array_mails ' + array_mails)
    return array_mails;
}

//https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


module.exports.checkEmail = checkEmail;
module.exports.validateEmail = validateEmail;