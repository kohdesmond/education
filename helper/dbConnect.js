//Create SQL connections
var mysql = require('mysql');
var conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password', 
    database: 'education'
});
conn.connect(); //Create SQL connection

module.exports = conn;
