var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

router.use(bodyParser.json()); // support parsing of application/json type post data
router.use(bodyParser.urlencoded({ extended: true })) ;//support parsing of application/x-www-form-urlencoded post data

//required controllers.
var register = require('./registerStudent'); //1.
var common = require('./commonstudents');//2.
var suspend = require('./suspendStudent');//3.
var retrieve = require('./receiveNotification');//4.

//1. As a teacher, I want to register one or more students to a specified teacher.
router.post('/register',register.Students);

//2. As a teacher, I want to retrieve a list of students common to a given list of teachers (i.e. retrieve students who are registered to ALL of the given teachers).
router.get('/commonstudents', common.studentsSearch);

//3. As a teacher, I want to retrieve a list of students common to a given list of teachers (i.e. retrieve students who are registered to ALL of the given teachers).
router.post('/suspend',suspend.student_With_Email);

//4. As a teacher, I want to retrieve a list of students who can receive a given notification.
router.post('/retrievefornotifications', retrieve.for_notifications);

module.exports = router;