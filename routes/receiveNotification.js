var conn = require('../helper/dbConnect.js');
var helper = require('../helper/helper_js.js');

exports.for_notifications = function(req, res) 
{
    let teacher = req.body.teacher;
    let notification = req.body.notification;
    console.log(req.body)
    //if null return invalid respone
    if (!teacher || !notification) 
    {
        return res.status(400).send({ message: "Invalid Request!"});
    }

    var mentionedStudentsArray = helper.checkEmail(notification); //Find out the email addresses in notification

    var recepients = [];// sql result array

    //MUST NOT be suspended, 
    //AND MUST fulfill AT LEAST ONE of the following: is registered with “teacherken@example.com" OR has been @mentioned in the notification
    var sql = "select distinct studentEmail from students where NOT isSuspended and (teacherEmail = ? or studentEmail in (?))"
    conn.query(sql,[teacher, mentionedStudentsArray], function(error, results, fields) 
    {    
        if(error) throw error;

        console.log(results)

        for(var i=0; i<results.length; i++) 
        {
            recepients.push(results[i].studentEmail);
        }

        console.log(recepients)
        console.log(recepients.length)


        if(recepients.length > 0)
        {   
            return res.status(200).json({ recepients : recepients });
        }
        else
        {
            return res.status(200).send({ message: 'No records found!'});
        }
    });
};





