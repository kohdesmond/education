var conn = require('../helper/dbConnect.js');


exports.studentsSearch = function(req,res) 
{
    var teacherEmail = req.query.teacher;
    console.log('\n\n\n' + teacherEmail + '\n\n\n');

    //Check null
    if (!teacherEmail) 
    {
        return res.status(400).json({message: "One or more of the email is invalid"});
    }

    console.log('\n\n\n' + 'count ' + teacherEmail.length + '\n\n\n');

    console.log('\n\n\n' + (typeof teacherEmail) === 'string' ? '1' : teacherEmail.length + '\n\n\n');

    //Select distinct student from DB
    //IN allows you to specify multiple values in a WHERE clause for teacherEmail
    //Group By to aggregate for having
    //Having to show only common students that are registered with distinct teachers by count if is multiple or single
    let sql = "select distinct studentEmail from students where teacherEmail IN (?) group by studentEmail having count(distinct teacherEmail) = ? ";

    conn.query(sql, [teacherEmail, (typeof teacherEmail) === 'string' ? '1' : teacherEmail.length ], function(error, results, fields) 
        {
            try 
            {
                if(error)
                {
                    throw error;
                }
                else
                {
                    var studentList = []; // empty array

                    for(var i = 0; i < results.length; i++) // loop 
                    {
                        studentList.push(results[i].studentEmail)
                    }

                    if(studentList.length > 0)
                    {
                        return res.status(200).json({ students: studentList });
                    }
                    else
                    {
                        return res.status(400).json({ message: 'Records not found!'});
                    }
                }
            } 
            catch (error) 
            {
                console.log(error.message);
                res.status(400).json({ message: "Database error"});
            }
        
    });
};