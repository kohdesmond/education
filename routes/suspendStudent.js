//Create SQL connections
var conn = require('../helper/dbConnect.js');

exports.student_With_Email = function(req, res) 
{
    let student = req.body.student;

    if (!student) 
    {
        return res.status(400).json({ message: "missing Student email"});
    }

    console.log(student);

    let sql = "update students set isSuspended = 1 where studentEmail = ?";
    conn.query(sql, student, function(error, results, fields) 
    {
        if(error) throw error;

        if(results.affectedRows > 0)
        {
            console.log("Number of records affected: " + results.affectedRows);
            return res.sendStatus(204);
        }
        else
        {
            return res.status(200).send({ message: "Record not found!"});
        }
    });
};