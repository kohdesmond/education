//Create SQL connections
// var mysql = require('mysql');
// var conn = mysql.createConnection({
//     host: 'localhost',
//     user: 'root',
//     password: 'password', 
//     database: 'education'
// });

var conn = require('../helper/dbConnect.js');
var helper = require('../helper/helper_js.js');

exports.Students = function(req, res) 
{
    //console.log(req.body);
    let teacher = req.body.teacher;
    let students = req.body.students;

    //if either field is null return error response
    if(!teacher || !students) 
    {
        return res.status(400).json({message: "missing Teacher or student email"});
    }

    var student_records = [];
    //var studentArray = students.split(",");
    var studentArray = helper.checkEmail(students);
    
    console.log(studentArray);

    for(var i = 0; i< studentArray.length; i++) 
    {   
        //if invalid email id return invalid response
        if(helper.validateEmail(studentArray[i]) && helper.validateEmail(teacher))
        {   
            var data = [studentArray[i], teacher];
            student_records.push(data);
        }
        else
        {
            return res.status(400).json({ message: "One or more of the email is invalid"});
        }
    }
        console.log(student_records);

        let sql = "INSERT INTO students (studentEmail,teacherEmail) VALUES ?";// values from student_records

        conn.query(sql, [student_records], function (error, result) 
        {

             try {
                if(error)
                {
                    throw error;
                }
                else
                {
                    console.log("Number of records inserted: " + result.affectedRows);
                    return res.sendStatus(204);
                }
            } 
            catch (error) 
            {
                console.log(error.message);
                return res.status(400).json({ message: "Duplicate entry found"});
            }
        });
};


