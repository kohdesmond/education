# Education

# Initial Set up
Assuming mysql was installed with DataGrip as the viewer
    
    host: 'localhost',
    user: 'root',
    password: 'password', 
    database: 'education'
    
    Create Schema: CREATE SCHEMA education;
    Create DB:
    create table students
    (
    id int auto_increment
    primary key,
    teacherEmail varchar(100) null,
    studentEmail varchar(100) null,
    isSuspended tinyint(1) default '0' null,
    constraint m2m
    unique (teacherEmail, studentEmail)
    )
    ;
    


# Install Dependencies
    #Go to the folder
    npm install mysql –save
    npm install express-generator –save
    npm install -g nodemon
    npm install body-parser
    
# To run the app
    npm start

# URLs
http://localhost:9000/api/suspend

http://localhost:9000/api/register

http://localhost:9000/api/commonstudents?teacher=teacherken%40gmail.com

http://localhost:9000/api/commonstudents?teacher=teacherken%40gmail.com&teacher=teacherDK%40gmail.com

http://localhost:9000/api/suspend

http://localhost:9000/api/retrievefornotifications
    
# PostMan Links
https://www.getpostman.com/collections/50d9042938309d37a410





